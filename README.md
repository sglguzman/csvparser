# cvsparser
###### Sergio Guzman-Lara

### Overview

This is my implementation for the Coding exercise (number 6). For reference here is what I used:

* Spring Tool Suite version 3.8.1.RELEASE, with Buildship plugin for gradle
* java version 1.8.0_211
* gradle (when running from command line) version 5.5.1

### Assumptions

    * Any input line with any missing/invalid/empty field is skipped (all line must have 4 fields)
    * The userId is case sensitive
    * Insurance-company and user-name are converted to upper-case
    * Any period (".") in the input is treated as a space
    * About first and last names:
        * Fields can be quoted to include a comma (see src/test/resources/test_file_1.txt for examples). 
        * If a comma is included, then we assume the format is 'Last, First'. 
        * This allows to define multi-word last names, for example:
            * "Van Winkle, George W." will become firstName[GEORGE W], lastName[VAN WINKLE]
         
### How to run

    * git clone https://gitlab.com/sglguzman/csvparser.git
    * cd csvparser
    * gradle clean build executableJar
    * java -jar build/libs/csvparser-executable-1.0.jar

This will print out usage information for the executable jar
