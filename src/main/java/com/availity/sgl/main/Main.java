package com.availity.sgl.main;

import com.availity.sgl.csv.parser.ProcessFile;

public class Main {

	public static void main(String[] args) {
		if (args.length != 2) {
			System.out.println("Usage:\n");
			System.out.println("java -jar csvparser-executable-1.0.jar <input_csv_file> <outputDirectory>\n\n");
			System.out.println("This will parse entries from <input_csv_file> and process them accordingly");
			System.out.println("to exercise (6).");
			System.out.println("The output files will be created in <outputDirectory>\n");
			System.exit(1);
		}
		if (ProcessFile.processFile(args[0],args[1])) {
			System.out.println("\n>>>>> Success");
		} else {
			System.out.println("\n>>>> Error!");
		}
	}

}
