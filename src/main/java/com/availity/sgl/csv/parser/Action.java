package com.availity.sgl.csv.parser;

public enum Action {
	AppendChar,
	NoOp,
	AppendField,
	AppendSpace
}
