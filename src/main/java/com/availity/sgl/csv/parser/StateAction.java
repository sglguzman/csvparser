package com.availity.sgl.csv.parser;

public class StateAction {
	State nextState;
	Action action;
	
	public StateAction(State s, Action a) {
		this.nextState = s;
		this.action = a;
	}
}
