package com.availity.sgl.csv.parser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Parser {
	static private Logger LOGGER = LogManager.getLogger(Parser.class);
	
	private HashMap<State,HashMap<Token,StateAction>> dfa;
	private boolean errorFound;
	
	public Parser() {
		errorFound = false;
		initialize();
	}

	public List<String> parseLine(String line) {
		errorFound = false;
		ArrayList<String> fields = new ArrayList<>();
		
		State currentState = State.Start;
		StringBuilder b = null;
		char ch;
		Token token;
		HashMap<Token,StateAction> transitions;
		StateAction sa;
		for (int i=0;i<line.length();i++) {
			ch = line.charAt(i);
			token = Token.getToken(ch);
			transitions = dfa.get(currentState);
			sa = transitions.get(token);
			switch (sa.action) {
			case AppendChar:
				if (b == null) {
					b = new StringBuilder();
				}
				b.append(ch);
				break;
			case NoOp:
				break;
			case AppendField:
				if (b == null ) {
					errorFound = true;
					LOGGER.warn("Failed to parse line [{}]",line);
					return new ArrayList<String>();	
				}
				if (b != null) {
					trimSpaces(b);
					if (b.length()==0) {
						errorFound = true;
						LOGGER.warn("Failed to parse line [{}]",line);
						return new ArrayList<String>();						
					}
					fields.add(b.toString());
				}
				b = null;
				break;
			case AppendSpace:
				if (b == null) {
					b = new StringBuilder();
				}
				if (b.length()>0 && b.charAt(b.length()-1) != ' ') {
					// Avoid multiple spaces
					b.append(' ');
				}
				break;
			default:
				break;
			}
			currentState = sa.nextState;
			if (currentState == State.Error) {
				errorFound = true;
				LOGGER.warn("Failed to parse line [{}]",line);
				return new ArrayList<String>();
			}
		}
		if (b != null) {
			trimSpaces(b);
			if (b.length()==0) {
				errorFound = true;
				LOGGER.warn("Failed to parse line [{}]",line);
				return new ArrayList<String>();						
			}
			fields.add(b.toString());
		}
		return fields;
	}
	
	public boolean parseSuccessful() {
		return !errorFound;
	}
	
	private void trimSpaces(StringBuilder b) {
		//trailing spaces
		while (b.length()>0 && b.charAt(b.length()-1)==' ') {
			b.deleteCharAt(b.length()-1);
		}
		//leading spaces
		while (b.length()>0 && b.charAt(0)==' ') {
			b.deleteCharAt(0);
		}
	}
	
	
	private void initialize() {
		dfa = new HashMap<>();
		
		//This is a DFA (Deterministic Finite Automaton) to handle quoted entries in the input
		
		//Transitions from State.Start:
		HashMap<Token,StateAction> tr = new HashMap<>();
		tr.put(Token.Comma, new StateAction(State.Error,Action.NoOp));
		tr.put(Token.Quote, new StateAction(State.Quoted,Action.NoOp));
		tr.put(Token.Other, new StateAction(State.Field,Action.AppendChar));
		tr.put(Token.Space, new StateAction(State.Start,Action.NoOp)); //trim leading spaces
		
		dfa.put(State.Start, tr);
		
		//Transitions from State.Quoted:
		tr = new HashMap<>();
		tr.put(Token.Comma, new StateAction(State.Quoted,Action.AppendChar));
		tr.put(Token.Quote, new StateAction(State.NeedComma,Action.AppendField));
		tr.put(Token.Other, new StateAction(State.Quoted,Action.AppendChar));
		tr.put(Token.Space, new StateAction(State.Quoted,Action.AppendSpace));
		
		dfa.put(State.Quoted, tr);

		//Transitions from State.NeedComma:
		tr = new HashMap<>();
		tr.put(Token.Comma, new StateAction(State.Start,Action.NoOp));
		tr.put(Token.Quote, new StateAction(State.Error,Action.NoOp));
		tr.put(Token.Other, new StateAction(State.Error,Action.NoOp));
		tr.put(Token.Space, new StateAction(State.NeedComma,Action.NoOp));
		
		dfa.put(State.NeedComma, tr);

		//Transitions from State.Field:
		tr = new HashMap<>();
		tr.put(Token.Comma, new StateAction(State.Start,Action.AppendField));
		tr.put(Token.Quote, new StateAction(State.Field,Action.AppendChar));
		tr.put(Token.Other, new StateAction(State.Field,Action.AppendChar));
		tr.put(Token.Space, new StateAction(State.Field,Action.AppendSpace));
		
		dfa.put(State.Field, tr);
	}
}
