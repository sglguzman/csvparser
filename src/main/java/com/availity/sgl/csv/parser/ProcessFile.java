package com.availity.sgl.csv.parser;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ProcessFile {
	static private Logger LOGGER = LogManager.getLogger(ProcessFile.class);
	
	static public boolean processFile(String fileName, String outputDirectory) {
		boolean ok = true;
		try {
			HashMap<String,HashMap<String,Entry>> insuranceMap = getInsuranceMap(fileName);
			Iterator<String> insurances = insuranceMap.keySet().iterator();
			while (insurances.hasNext()) {
				String insuranceName = insurances.next();
				String fName = convertToFileName(insuranceName);
				File f = new File(outputDirectory,fName);
				createFile(f.getAbsolutePath(),insuranceMap.get(insuranceName));
				
			}
		} catch (Exception e) {
			LOGGER.error("Failed to proecess file [{}], error [{}]",fileName,e.getMessage());
			ok = false;
		}
		return ok;
	}
	
	private static void createFile(String fileName, HashMap<String,Entry> entries) throws FileNotFoundException {
		PrintWriter out = new PrintWriter(new FileOutputStream(fileName));
		ArrayList<Entry> sorted = new ArrayList<>(entries.values());
		Collections.sort(sorted);
		
		for (Entry e:sorted) {
			out.print(e.getUserId());
			out.print(",\"");
			out.print(e.getLastName());
			out.print(",");
			out.print(e.getFirstName());
			out.print("\",");
			out.print(e.getVersion());
			out.print(",");
			out.println(e.getInsuranceCompany());
		}
		out.close();
	}
	private static HashMap<String,HashMap<String,Entry>>getInsuranceMap(String fName) throws IOException {
		BufferedReader in = new BufferedReader(new FileReader(fName));
		String line;
		Parser parser = new Parser();
		// map InsuranceCompany,userId => Entry
		HashMap<String,HashMap<String,Entry>> insuranceMap = new HashMap<>();
		
		while ((line = in.readLine())!= null) {
			if (line.length()>0) {
				List<String> fields = parser.parseLine(line);
				if (parser.parseSuccessful()) {
					Entry e = new Entry(fields);
					if (e.isValid()) {
						HashMap<String,Entry> map = insuranceMap.get(e.getInsuranceCompany());
						if (map == null) {
							map = new HashMap<>();
							map.put(e.getUserId(), e);
							insuranceMap.put(e.getInsuranceCompany(), map);
						} else {
							Entry old = map.get(e.getUserId());
							if (old != null) {
								LOGGER.info("Found duplicated entries [{},\"{}\",{},{}] and [{},\"{}\",{},{}], choosing the higher version",
										e.getUserId(),
										e.getLastName()+","+e.getFirstName(),
										e.getVersion(),
										e.getInsuranceCompany(),
										old.getUserId(),
										old.getLastName()+","+e.getFirstName(),
										old.getVersion(),
										old.getInsuranceCompany());
							}
							if (old == null || e.getVersion()>old.getVersion()) {
								map.put(e.getUserId(),e);
							}
						}
					}
				}
			}
		}
		in.close();
		return insuranceMap;
	}
	
	private static String convertToFileName(String s) {
		StringBuilder b = new StringBuilder();
		for (int i=0;i<s.length();i++) {
			char ch = s.charAt(i);
			if (ch == ' ') {
				ch = '_';
			}
			b.append(ch);
		}
		b.append(".csv");
		return b.toString();
	}
}
