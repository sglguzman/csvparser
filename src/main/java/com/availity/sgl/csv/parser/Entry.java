package com.availity.sgl.csv.parser;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Entry implements Comparable<Entry> {
	static private Logger LOGGER = LogManager.getLogger(Entry.class);
	
	private String userId;
	private String firstName;
	private String lastName;
	private int version;
	private String insuranceCompany;
	
	private boolean valid;
	
	public Entry(List<String> args) {
		valid = true;
		if (args.size() != 4) {
			LOGGER.warn("Expecting 4 fields, but got [{}], skipping this entry",args.size());
			valid = false;
		}
		if (valid) {
			userId = args.get(0).trim();
			setNames(args.get(1).toUpperCase().trim());
			setVersion(args.get(2).trim());
			insuranceCompany = args.get(3).toUpperCase().trim();	
		}
	}
	
	public boolean isValid() {
		return valid;
	}
	
	public String getUserId() {
		return userId;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public int getVersion() {
		return version;
	}

	public String getInsuranceCompany() {
		return insuranceCompany;
	}
	
	private void setVersion(String s) {
		if (!valid) {
			return;
		}
		try {
			version = Integer.parseInt(s);
		} catch (NumberFormatException e) {
			LOGGER.warn("Failed to parse [{}] as integer, skipping entry",s);
			valid = false;
		}
	}

	private void setNames(String s) {
		if (!valid) {
			return;
		}
		int i = s.indexOf(',');
		boolean error = false;
		try {
			if (i>=0) {
				// format: Last, First
				lastName = s.substring(0, i).trim();
				firstName = s.substring(i+1,s.length()).trim();
			} else {
				// format: First Last
				String[] parts = s.split(" ");
				lastName = parts[parts.length-1];
				StringBuilder b = new StringBuilder();
				for (int j=0;j<parts.length-1;j++) {
					if (b.length()>0) {
						b.append(' ');
					}
					b.append(parts[j]);
				}
				firstName = b.toString();
			}
			if (firstName.length()==0 || lastName.length()==0) {
				error = true;
			}
		} catch (Exception e) {
			error = true;
		}
		if (error) {
			LOGGER.warn("Failed to parse [{}] as 'LastName,FirstName' or 'FirstName LastName', skipping entry",s);
			valid = false;
		}
	}

	@Override
	public int compareTo(Entry other) {
		int first = this.getLastName().compareTo(other.getLastName());
		
		if (first == 0) {
			return this.getFirstName().compareTo(other.firstName);
		}
		
		return first;
	}
}
