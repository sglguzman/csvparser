package com.availity.sgl.csv.parser;

public enum State {
	Start,
	Quoted,
	Field,
	NeedComma,
	Error
}
