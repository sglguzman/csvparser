package com.availity.sgl.csv.parser;

public enum Token {
	Quote,
	Comma,
	Space,
	Other;
	
	
	public static Token getToken(char ch) {
		if (ch == ',') {
			return Comma;
		}
		if (ch == '"') {
			return Quote;
		}
		if (ch == ' ' || ch =='\t' || ch == '.') {
			return Space;
		}
		return Other;
	}
}
