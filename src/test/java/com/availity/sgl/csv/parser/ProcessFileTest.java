package com.availity.sgl.csv.parser;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.availity.sgl.csv.parser.ProcessFile;

public class ProcessFileTest {

	@Test
	public void test() {
		deleteFile("src/test/resources/INSURANCE_A_CO.csv");
		deleteFile("src/test/resources/OTHER_CO.csv");
		ProcessFile.processFile("src/test/resources/test_file_1.txt", "src/test/resources/");
		List<String> other = readFile("src/test/resources/OTHER_CO.csv");
		assertNotNull("File [src/test/resources/OTHER_CO.csv] was not created",other);

		List<String> insA = readFile("src/test/resources/INSURANCE_A_CO.csv");
		assertNotNull("File [src/test/resources/INSURANCE_A_CO.csv] was not created",insA);
		
		assertEquals(3, other.size());
		assertEquals("001,\"GUZMAN LARA,SERGIO\",1,OTHER CO",other.get(0));
		assertEquals("015,\"SMITH,JOHN A\",4,OTHER CO",other.get(1));
		assertEquals("010,\"SMITH,JOHN A\",4,OTHER CO",other.get(2));
		
		assertEquals(3, insA.size());
		assertEquals("030,\"GUZMAN LARA,ALEX\",20,INSURANCE A CO",insA.get(0));
		assertEquals("001,\"GUZMAN LARA,SERGIO\",15,INSURANCE A CO",insA.get(1));
		assertEquals("011,\"VAN WINKLE,JOHN W C\",10,INSURANCE A CO",insA.get(2));
	}

	private List<String> readFile(String fname) {
		try {
			ArrayList<String> lines = new ArrayList<>();
			BufferedReader in = new BufferedReader(new FileReader(fname));
			String line;
			
			while ((line = in.readLine())!= null) {
				lines.add(line);
			}
			in.close();
			return lines;
		} catch (IOException e) {
			return null;
		}
	}
	private void deleteFile(String fname) {
		File f = new File(fname);
		
		if (f.exists()) {
			f.delete();
		}
	}
}
