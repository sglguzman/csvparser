package com.availity.sgl.csv.parser;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;

public class EntryTest {

	@Test
	public void test1() {
		List<String> a = Arrays.asList("id","john george smith","300","some insurance co");
		
		Entry e = new Entry(a);
		assertEquals(true,e.isValid());
		assertEquals("id",e.getUserId());
		assertEquals("JOHN GEORGE",e.getFirstName());
		assertEquals("SMITH",e.getLastName());
		assertEquals(300,e.getVersion());
		assertEquals("SOME INSURANCE CO",e.getInsuranceCompany());
	}

	@Test
	public void test2() {
		List<String> a = Arrays.asList("id","smith,john george","300","some insurance co");
		
		Entry e = new Entry(a);
		assertEquals(true,e.isValid());
		assertEquals("id",e.getUserId());
		assertEquals("JOHN GEORGE",e.getFirstName());
		assertEquals("SMITH",e.getLastName());
		assertEquals(300,e.getVersion());
		assertEquals("SOME INSURANCE CO",e.getInsuranceCompany());
	}

	@Test
	public void test3() {
		List<String> a = Arrays.asList("id","smith,john george","300","some insurance co");
		
		Entry e = new Entry(a);
		assertEquals(true,e.isValid());
		assertEquals("id",e.getUserId());
		assertEquals("JOHN GEORGE",e.getFirstName());
		assertEquals("SMITH",e.getLastName());
		assertEquals(300,e.getVersion());
		assertEquals("SOME INSURANCE CO",e.getInsuranceCompany());
	}

	@Test
	public void test4() {
		List<String> a = Arrays.asList("id","van winkle,john r george","300","some insurance co");
		
		Entry e = new Entry(a);
		assertEquals(true,e.isValid());
		assertEquals("id",e.getUserId());
		assertEquals("JOHN R GEORGE",e.getFirstName());
		assertEquals("VAN WINKLE",e.getLastName());
		assertEquals(300,e.getVersion());
		assertEquals("SOME INSURANCE CO",e.getInsuranceCompany());
	}

	@Test
	public void testInvalid1() {
		List<String> a = Arrays.asList("id","van winkle,john r george","300.1","some insurance co");
		
		Entry e = new Entry(a);
		assertEquals(false,e.isValid());
	}

	@Test
	public void testInvalid2() {
		List<String> a = Arrays.asList("id",",john r george","300","some insurance co");
		
		Entry e = new Entry(a);
		assertEquals(false,e.isValid());
	}

	@Test
	public void testInvalid3() {
		List<String> a = Arrays.asList("id","john","300","some insurance co");
		
		Entry e = new Entry(a);
		assertEquals(false,e.isValid());
	}

	@Test
	public void testComparable1() {
		List<String> a = Arrays.asList("id","john,adams","300","some insurance co");
		List<String> b = Arrays.asList("id","john,boone","300","some insurance co");
		
		Entry e1 = new Entry(a);
		Entry e2 = new Entry(b);
		int i = e1.compareTo(e2);
		assertEquals(-1,i);
	}

	@Test
	public void testComparable2() {
		List<String> a = Arrays.asList("id","john,adams","300","some insurance co");
		List<String> b = Arrays.asList("id","johna,adams","300","some insurance co");
		
		Entry e1 = new Entry(a);
		Entry e2 = new Entry(b);
		int i = e1.compareTo(e2);
		assertTrue(i<0);
	}

	@Test
	public void testComparable3() {
		List<String> a = Arrays.asList("id","john,adams","300","some insurance co");
		List<String> b = Arrays.asList("id","john,adams","300","some insurance co");
		
		Entry e1 = new Entry(a);
		Entry e2 = new Entry(b);
		int i = e1.compareTo(e2);
		assertEquals(0,i);
	}

	@Test
	public void testComparable4() {
		List<String> a = Arrays.asList("id","john,addms","300","some insurance co");
		List<String> b = Arrays.asList("id","john,adams","300","some insurance co");
		
		Entry e1 = new Entry(a);
		Entry e2 = new Entry(b);
		int i = e1.compareTo(e2);
		assertTrue(i>0);
	}

	@Test
	public void testComparable5() {
		List<String> a = Arrays.asList("id","joho,adams","300","some insurance co");
		List<String> b = Arrays.asList("id","john,adams","300","some insurance co");
		
		Entry e1 = new Entry(a);
		Entry e2 = new Entry(b);
		int i = e1.compareTo(e2);
		assertTrue(i>0);
	}

}
