package com.availity.sgl.csv.parser;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;

public class ParserTest {

	Parser p = new Parser();
	
	@Test
	public void testNoQuotes1() {
		
		List<String> fields = p.parseLine("One,Two,Three");
		assertEquals(true,p.parseSuccessful());
		assertEquals(3,fields.size());
		assertEquals("One",fields.get(0));
		assertEquals("Two",fields.get(1));
		assertEquals("Three",fields.get(2));		
	}

	@Test
	public void testNoQuotes2() {
		List<String> fields = p.parseLine("One, Two,Three ");
		assertEquals(true,p.parseSuccessful());
		assertEquals(3,fields.size());
		assertEquals("One",fields.get(0));
		assertEquals("Two",fields.get(1));
		assertEquals("Three",fields.get(2));		
	}

	@Test
	public void testNoQuotes3() {
		List<String> fields = p.parseLine(" One, Two,Three ");
		assertEquals(true,p.parseSuccessful());
		assertEquals(3,fields.size());
		assertEquals("One",fields.get(0));
		assertEquals("Two",fields.get(1));
		assertEquals("Three",fields.get(2));		
	}

	@Test
	public void testNoQuotes4() {
		List<String> fields = p.parseLine(" One, Two and other ,Three ");
		assertEquals(true,p.parseSuccessful());
		assertEquals(3,fields.size());
		assertEquals("One",fields.get(0));
		assertEquals("Two and other",fields.get(1));
		assertEquals("Three",fields.get(2));		
	}

	@Test
	public void testNoQuotes5() {
		List<String> fields = p.parseLine(" One, Two  and  other ,Three ");
		assertEquals(true,p.parseSuccessful());
		assertEquals(3,fields.size());
		assertEquals("One",fields.get(0));
		assertEquals("Two and other",fields.get(1));
		assertEquals("Three",fields.get(2));		
	}

	@Test
	public void testNoQuotes6() {
		List<String> fields = p.parseLine(" One, Two R. Good,Three ");
		assertEquals(true,p.parseSuccessful());
		assertEquals(3,fields.size());
		assertEquals("One",fields.get(0));
		assertEquals("Two R Good",fields.get(1));
		assertEquals("Three",fields.get(2));		
	}

	@Test
	public void testNoQuotesError1() {
		List<String> fields = p.parseLine(" One, Two,,Three ");
		assertEquals(0,fields.size());
		assertEquals(false,p.parseSuccessful());
	}

	@Test
	public void testNoQuotesError2() {
		List<String> fields = p.parseLine(" One, Two,Tree,, ");
		assertEquals(0,fields.size());
		assertEquals(false,p.parseSuccessful());
	}

	@Test
	public void testNoQuotesError3() {
		List<String> fields = p.parseLine(" , , Two,Tree,, ");
		assertEquals(0,fields.size());
		assertEquals(false,p.parseSuccessful());
	}

	@Test
	public void testQuotes1() {
		List<String> fields = p.parseLine(" One,\" Two times\",Three ");
		assertEquals(true,p.parseSuccessful());
		assertEquals(3,fields.size());
		assertEquals("One",fields.get(0));
		assertEquals("Two times",fields.get(1));
		assertEquals("Three",fields.get(2));		
	}

	@Test
	public void testQuotes2() {
		List<String> fields = p.parseLine(" One, \" Two times\" ,Three ");
		assertEquals(true,p.parseSuccessful());
		assertEquals(3,fields.size());
		assertEquals("One",fields.get(0));
		assertEquals("Two times",fields.get(1));
		assertEquals("Three",fields.get(2));		
	}

	@Test
	public void testQuotes3() {
		List<String> fields = p.parseLine(" One, \" Two, times\",Three ");
		assertEquals(true,p.parseSuccessful());
		assertEquals(3,fields.size());
		assertEquals("One",fields.get(0));
		assertEquals("Two, times",fields.get(1));
		assertEquals("Three",fields.get(2));		
	}

	@Test
	public void testQuotes4() {
		List<String> fields = p.parseLine(" One, \" Two, times\"   ,Three ");
		assertEquals(true,p.parseSuccessful());
		assertEquals(3,fields.size());
		assertEquals("One",fields.get(0));
		assertEquals("Two, times",fields.get(1));
		assertEquals("Three",fields.get(2));		
	}
	
	@Test
	public void testQuotes5() {
		List<String> fields = p.parseLine(" One, \" Two,   times  \"   ,Three ");
		assertEquals(true,p.parseSuccessful());
		assertEquals(3,fields.size());
		assertEquals("One",fields.get(0));
		assertEquals("Two, times",fields.get(1));
		assertEquals("Three",fields.get(2));		
	}

	@Test
	public void testQuotes6() {
		List<String> fields = p.parseLine(" One, \" Two, G.W.  \"   ,Three ");
		assertEquals(true,p.parseSuccessful());
		assertEquals(3,fields.size());
		assertEquals("One",fields.get(0));
		assertEquals("Two, G W",fields.get(1));
		assertEquals("Three",fields.get(2));		
	}

	@Test
	public void testQuotesError1() {
		List<String> fields = p.parseLine(" One, \" \",Three ");
		assertEquals(0,fields.size());
		assertEquals(false,p.parseSuccessful());
	}

	@Test
	public void testQuotesError2() {
		List<String> fields = p.parseLine(" One, \" something\",\"\"");
		assertEquals(0,fields.size());
		assertEquals(false,p.parseSuccessful());
	}

}
